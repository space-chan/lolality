import asyncio
from os import name
import discord
from discord import channel
from discord import voice_client 
from discord.ext import commands, tasks
import youtube_dl
from random import choice
import debug

from youtube_dl.YoutubeDL import YoutubeDL 

youtube_dl.utils.bug_reports_message = lambda: ''

ytldl_format_options = {
    'format': 'bestaudio/best',
    'outtml': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0'
}

ffmpeg_options = {
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytldl_format_options)

class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get('title')
        self.url = data.get('url')

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=True):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))

        if 'entries' in data:
            data = data['entries'][0]

        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)


idle = True
mark = "**"
prefix = "."

client = commands.Bot(command_prefix=prefix)

status = [
    'Manacand.',
    'Dormind.',
    'Muzica',
    '.help pentru ajutor'
]

@client.event
async def on_ready():
    print('Trezirea botului..')
    print('Pregatirea statusului...')
    await change_status()
    print(f"{client.user.name} este gata.")

@client.event
async def on_member_join(member):
    channel = discord.utils.get(member.guild.channels, name="general")
    await channel.send(f"Bun venit {member.mention}! Gata sa ma folosesti? Scrie {prefix}help sa imi vezi comenzile.")

@client.command(name='ping', help='Aceasta comanda arata timpul de raspuns al botului.')
async def ping(ctx):
    await ctx.send(f"**Pong! Timp de raspuns: {round(client.latency * 1000)} ms**")

@client.command(name="salut", help="Spune salut botului si vezi ce iti raspunde.")
async def salut(ctx):
    if idle == False:
        raspunsuri = [
            'De ce m-ai trezi? Lasa-ma sa dorm.',
            'Salut.',
            '.....',
            'Nu sunt aici.',
            'Am treaba.'
        ]
    elif idle == False:
        raspunsuri = ['Sunt ocupat, dar salut.']
        await ctx.send(f"{mark}{choice(raspunsuri)}{mark}")

@client.command(name='play', help='Porneste muzica intr-un vc. (Trebuie sa dai stop inainte de-a mai da play la alta muzica)')
async def play(ctx, *, url):
    debug.info(url)
    replay_url = url
    idle = False
    if not ctx.message.author.voice:
        await ctx.send('Nu esti intr-un vc')
        return 
    else:
        channel = ctx.message.author.voice.channel
    
    await channel.connect()

    server = ctx.message.guild
    voice_channel = server.voice_client

    async with ctx.typing():
        player = await YTDLSource.from_url(url, loop=client.loop)
        voice_channel.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
    await ctx.send(f"{mark}Acum dau play la:{mark} __{player.title}__ \n{mark}Likes:{mark} __{player.data.get('like_count')}__ {mark}Dislikes:{mark} __{player.data.get('dislike_count')}__")
    await change_status_music(player)

@client.command(name='stop', help='Opreste muzica dintr-un vc si pleaca.')
async def stop(ctx):
    await change_status()
    if ctx.message.guild.voice_client:
         voice_client = ctx.message.guild.voice_client
    else:
        await ctx.send('Nu sunt intr-un vc.')
        return

    await voice_client.disconnect()

@client.command(name='credits', help='Creditele')
async def credits(ctx):
    await ctx.send(f"{mark}Acest bot este facut de space-chan.{mark}")

@tasks.loop(seconds=20)
async def change_status():
        await client.change_presence(activity=discord.Game(choice(status)))

async def change_status_music(player):
    await client.change_presence(activity=discord.Game(f"Muzica: {player.title}"))

client.run("OTAyNDk1MjMwMjMyNzY4NTQy.YXfQLg.5ywWivNrMqX2XSuipKFrcTrbu6c")